<?php

namespace App\Console\Commands;

use App\Models\GuestbookEntry;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class GenerateEntriesReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:entry-report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate file with GuestBook entries';

    protected $tries = 0;

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->generateReport();
    }

    private function generateReport()
    {
        try {
            $entries = GuestbookEntry::orderBy('created_at', 'asc')->get();
            Storage::disk('local')->put('report.json', json_encode($entries));
            return 'true';
        } catch (\Throwable $th) {
            Log::channel('generate_reports')->error($th);
        }
    }
}
