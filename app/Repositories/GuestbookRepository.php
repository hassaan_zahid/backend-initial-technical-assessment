<?php

namespace App\Repositories;

use App\Events\DeleteEntry;
use App\Models\GuestbookEntry;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class GuestbookRepository
{

    public function __construct(private User $user)
    {
        // Simulate a logged-in user
        $this->user = User::find(1);
    }

    public function get()
    {
        return GuestbookEntry::orderBy('created_at', 'desc')->with('submitter')->get();
    }

    public function my()
    {
        return GuestbookEntry::whereHas('submitter', function($query) {
            return $query->where('email', $this->user->email);
        })->get();

    }

    public function store($data)
    {
        $data['submitter_id'] = $this->user->id;

        return GuestbookEntry::create($data);
    }

    public function update($entry, $data)
    {
        return $entry->update($data);
    }




    public function destroy($guestbook)
    {
        $record = GuestbookEntry::find($guestbook)->load('submitter');

        GuestbookEntry::destroy($record->id);

        event(new DeleteEntry($record->toArray()));
    }


}
