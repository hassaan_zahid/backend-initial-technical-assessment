<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\GuestbookEntryRequest;
use App\Models\GuestbookEntry;
use App\Repositories\GuestbookRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ApiGuestBookController extends Controller
{
    public function __construct(private GuestbookRepository $guestbookRepository)
    {}

    public function index()
    {
        return response()->json(['data' => $this->guestbookRepository->get()]);
    }

    public function my()
    {
        if (Auth::user() === null)
            return response()->json(['message' => 'Not logged in'], 401);

        return response()->json(['data' => $this->guestbookRepository->my()]);
    }


    public function show(GuestbookEntry $guestbook)
    {
        return response()->json(['data' => $guestbook]);
    }

    public function store(GuestbookEntryRequest $request)
    {
        try {
            $data = $this->guestbookRepository->store($request->validated());

            return response()->json(
                ['message' => 'Created!', 'data' => $data,]);
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    public function update(GuestbookEntry $guestbook, GuestbookEntryRequest $request)
    {
        if (auth('sanctum')->user()->id !== $guestbook->submitter_id)
            return response()->json(['message' => 'Unauthorized'], 401);

        try {
            $this->guestbookRepository->update($guestbook, $request->validated());

            return response()->json(
                ['message' => 'Updated!',]
            );
        } catch (\Throwable $th) {
            return response()->json([
                    'message' => $th->getMessage()],
                500
            );
        }
    }


    public function destroy($guestbook)
    {
        try {
            $this->guestbookRepository->destroy($guestbook);

            return response()->json(['message' => 'Deleted!']);
        } catch (\Throwable $th) {
            Log::error($th);

            return response()->json(['message' => $th->getMessage()], 500);
        }
    }
}
