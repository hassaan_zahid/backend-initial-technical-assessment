<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        $authBody = $request->validated();

        if (Auth::attempt($authBody)) {
            $token = Auth::user()->createToken('token');
            return response()->json(
                [
                    'message' => "Authenticated as {$request->user()->email}",
                    'token' => $token->plainTextToken,
                ]
            );
        }
        return response()
            ->json(
                [
                    'message' => "Invalid credentials: {$authBody['email']} / {$authBody['password']}"
                ],
                401
            );
    }


    public function whoami(Request $request)
    {
        if ($request->user() === null)
            return response()->json(['message' => 'Not logged in'], 401);
        return $request->user();
    }

    public function logout()
    {
        Auth::logout();
        return response()->json(['status' => true]);
    }
}
