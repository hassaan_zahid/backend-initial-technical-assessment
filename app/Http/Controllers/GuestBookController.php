<?php

namespace App\Http\Controllers;

use App\Http\Requests\GuestbookEntryRequest;
use App\Repositories\GuestbookRepository;
use Illuminate\Support\Facades\Log;

class GuestBookController extends Controller
{
    public function __construct(private GuestbookRepository $guestbookRepository)
    {

    }

    public function index()
    {
        return view('index', ["entries" => $this->guestbookRepository->get()]);
    }

    public function create()
    {
        return view('form');
    }

    public function store(GuestbookEntryRequest $request)
    {
        try {
            $this->guestbookRepository->store($request->validated());
            return redirect()->route('index')->with(['message' => 'Created successfully']);
        } catch (\Throwable $th) {
            return redirect()->back()->withErrors(['message' => $th->getMessage()]);
        }
    }

}
