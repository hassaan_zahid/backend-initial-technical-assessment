<?php

namespace App\Listeners;

use App\Events\DeleteEntry;
use App\Services\GuestbookEntryDeletionService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class EntryCleanup
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\DeleteEntry  $event
     * @return void
     */
    public function handle(DeleteEntry $event)
    {
        $service = new GuestbookEntryDeletionService();

        $service->notifyUserOfDeletion($event->data['submitter']['email']);
        $service->generateNewReport();
        $service->performCleanupTasks();
    }
}
