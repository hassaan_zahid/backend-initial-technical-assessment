# Comments

## 1. Routing logic

For this task, I refactored the logic from the routes to controllers and repositories, and I also created some form requests for validations.

For each route, I will create the following tests:

WEB:
- `index`:
    - Verify if it returns the index page.
    - Verify if it returns the index with data.

- `submit`:
    - Verify if it returns the submit page.

API:
- `index`:
    - Verify if it returns status 200.
    - Verify if it returns an empty array.
    - Verify if it returns an array.
    - Verify if it returns an array with entries.

- `whoami`:
    - Verify if it returns status 401.
    - Verify if the user is logged in.
    - Verify if it returns status 200.
    - Verify if it returns an empty array.
    - Verify if it returns an array.
    - Verify if it returns an array with entries.

- `show`:
    - Verify if it returns status 200.
    - Verify if it returns status 404.
    - Verify if it returns an entry.

- `destroy`:
    - Verify if it returns status 200.
    - Verify if it returns a success message.
    - Verify if it returns status 500.
    - Verify if it returns an error message.

- `store`:
    - Verify if the validations are correct.
    - Verify if it returns status 200.
    - Verify if it returns the created entry.
    - Verify if it returns a success message.
    - Verify if it returns status 500.
    - Verify if it returns an error message.


## 2. Completing the form

For this task, I extended the submit form to include additional fields for capturing all submitter information, and I created a new POST route to handle the form submission.

For the new route, I would add the following tests:
- Verify if it returns errors.
- Verify if it redirects to the index page.
- Verify if the redirect includes a success message.

I also used Form Request for validate data

## 3. Separating the submitter information from the `GuestbookEntry`

For this task, I made changes to the migrations and seeders. Also created a new migration for add columns to user table.

Additionally, I updated the `User` and `GuestbookEntry` models to establish relations between them. This allows for more structured data management and enhances the application's functionality. Furthermore, I modified the index page to utilize these relations and removed the submitter information from the submit form. Instead, I implemented a method in the `GuestbookRepository` to facilitate logging in first user, ensuring a consistent approach for testing the form.

Regarding tests, I would update those that return guestbook entries to verify whether the submitter information is included or excluded in the relations, depending on the requirements and design choices of the application.

## 4. Update an entry

For this task, I implemented the update methods in both the `ApiGuestbookController` and `GuestbookRepository`, and I added the corresponding update route. Additionally, I opted to utilize authentication tokens to facilitate testing of the update route with authenticated users.

For the new route, I would add the following tests:
- Verify if it returns status 401 (Unauthorized) when the user is not authenticated.
- Verify if it returns status 200 (OK) upon successful update.
- Verify if it returns a success message upon successful update.
- Verify if it returns status 500 (Internal Server Error) in case of an unexpected error.
- Verify if it returns an appropriate error message in case of failure.

These tests ensure that the update functionality behaves as expected under different scenarios and provides adequate error handling for potential issues.

## 5. Generate an hourly report

For this task, I opted to create a command to generate the report and set up a scheduler to run it every hour. I created a file named GenerateEntriesReport.

## 6. React to an entry being deleted

For this task, I decided to implement an event and listener to manage the deletion of an entry. This approach allows me to encapsulate this aspect of the logic in a single location, providing flexibility to add, remove, or modify tasks as needed.
