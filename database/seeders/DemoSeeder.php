<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\{GuestbookEntry, User};
use Illuminate\Support\Facades\Hash;

class DemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email'         => 'user-a@example.com',
            'password'      => Hash::make('user-a'),
            'real_name'     => 'User Alpha',
            'display_name'  => 'user-a',
        ]);

        User::create([
            'email'         => 'user-b@example.com',
            'password'      => Hash::make('user-b'),
            'real_name'     => 'User Bravo',
            'display_name'  => 'user-b',
        ]);

        $user = User::create([
            'email'         => 'the-bez@amazon.com',
            'password'      => Hash::make('space-cowboy'),
            'real_name'     => 'Beff Jezos',
            'display_name'  => 'TheBez',
        ]);

        GuestbookEntry::create([
            'title'         => 'This is really amazing',
            'content'       => 'Much better than Amazon',
            'submitter_id'  => $user->id,
        ]);

        $user = User::create([
            'email'         => 'egomaniac@tesla.com',
            'password'      => Hash::make('iOwnTwitter'),
            'real_name'     => 'Melon Dusk',
            'display_name'  => 'RocketMan',
        ]);

        GuestbookEntry::create([
            'title'         => 'Wow.',
            'content'       => 'This is so great that it sends me to space',
            'submitter_id'  => $user->id,
        ]);
    }
}
