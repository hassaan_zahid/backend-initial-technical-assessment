<?php

use App\Http\Controllers\Auth\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Auth, Route};

/*
|--------------------------------------------------------------------------
| Demo Auth Routes
|--------------------------------------------------------------------------
|
| These are not representative of how routes & logic would be represented
| in a real-world application
|
| Please do not touch these as part of the technical test!
|
*/

Route::middleware([\App\Http\Middleware\JsonContentType::class])->group(function () {
    Route::post('/login', [AuthController::class, 'login']);
});

Route::get('/whoami', [AuthController::class, 'whoami']);
Route::get('/logout', [AuthController::class, 'logout']);

