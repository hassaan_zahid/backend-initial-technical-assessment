<?php

use App\Http\Controllers\API\ApiGuestBookController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Routes for the guestbook API
 */
Route::prefix('/guestbook')->group(function () {
    Route::get('/', [ApiGuestbookController::class, 'index']);
    Route::get('/{guestbook}', [ApiGuestbookController::class, 'show']);

    Route::middleware(['auth:sanctum', \App\Http\Middleware\JsonContentType::class])->group(function () {
        Route::get('/my', [ApiGuestbookController::class, 'my']);
        Route::delete('/{guestbook}', [ApiGuestbookController::class, 'destroy']);
        Route::post('/', [ApiGuestbookController::class, 'store']);
        Route::put('/{guestbook}', [ApiGuestbookController::class, 'update']);
    });
});

